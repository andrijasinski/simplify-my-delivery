# App

* Official title - "Lähima pakiautomaadi leidja". 

* App is continuesly deployed to https://simplify-my-delivery.herokuapp.com/

* [Project proposal](https://courses.cs.ut.ee/MTAT.03.138/2017_fall/uploads/Main/LahimaPakiautomaadiLeidja.pdf)
* [Project plan](https://bitbucket.org/andrijasinski/simplify-my-delivery/wiki/Project%20plan)

* [How to build application from source code](https://bitbucket.org/andrijasinski/simplify-my-delivery/wiki/How%20to%20build%20project%20from%20source%20code)

* [How to run tests from source code](https://bitbucket.org/andrijasinski/simplify-my-delivery/wiki/How%20to%20run%20tests%20from%20source%20code)


# Requirements

* [Functional requirements](https://bitbucket.org/andrijasinski/simplify-my-delivery/wiki/1.%20Functional%20requirements)

* [Non-functional requirements](https://bitbucket.org/andrijasinski/simplify-my-delivery/wiki/2.%20Non-functional%20requirements)

# Iterations

* [1st iteration](https://bitbucket.org/andrijasinski/simplify-my-delivery/wiki/1st%20iteration)

* [2nd iteration](https://bitbucket.org/andrijasinski/simplify-my-delivery/wiki/2nd%20iteration)

* [3rd iteration](https://bitbucket.org/andrijasinski/simplify-my-delivery/wiki/3rd%20iteration)

* [4th iteration](https://bitbucket.org/andrijasinski/simplify-my-delivery/wiki/4th%20iteration)

# Peer review

* [Peer review](https://bitbucket.org/andrijasinski/simplify-my-delivery/wiki/Peer%20review)

* [Response to peer review](https://bitbucket.org/andrijasinski/simplify-my-delivery/wiki/Response%20to%20peer%20review)

# Summary
* [Project summary](https://docs.google.com/document/d/1sccwBg2mE9rK5lPL3w6KfLkR4QoYkRuSoLl_zUOMBSk/edit?usp=sharing)

# Team

* Team members: Andri Jasinski, Daniil Konovalov, Jan Moppel, Indrek Lään
* Customer Marita Mägi.
