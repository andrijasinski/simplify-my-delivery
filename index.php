<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Lähima pakiautomaadi leidja</title>
		<link rel="icon" type="image/x-icon" href="./public_html/images/logo_omniva.png">
		<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" 
		integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"-->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<link rel="stylesheet" type="text/css" href="public_html/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="public_html/css/main.css">
		<link rel="stylesheet" type="text/css" href="public_html/css/style.css">

		<script type="text/javascript" src="js/functions.js"></script>

		<?php 
			$urlextension = substr($_SERVER['HTTP_HOST'], -2);
			if($urlextension == "ee") {
				$lang = "et";
			} else if ($urlextension == "lt") {
				$lang = "lt";
			} else if ($urlextension == "lv") {
				$lang = "lv";
			} else {
				$lang = "en";
			}
			$languages = array("en", "ru", "et", "lv", "lt");
			if(isset($_GET['lang']) && in_array($_GET['lang'], $languages)) {
					$lang = $_GET['lang'];
			}
			$str = file_get_contents("./php/lang/lang_{$lang}.json");
			$json_lang = json_decode($str, true);
		?>

	</head>

	<body onload="onLoad()">
		
	<!-- Header -->
		<div class="row language">
			<div class="col-sm-12">
				<div class="dropdown d-flex justify-content-end align-items-center">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<?php echo strtoupper($lang); ?>
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">
						<a class="dropdown-item" href="index.php?lang=en">EN</a>
						<a class="dropdown-item" href = "index.php?lang=et">ET</a>
						<a class="dropdown-item" href = "index.php?lang=lv">LV</a>
						<a class="dropdown-item" href = "index.php?lang=lt">LT</a>
						<a class="dropdown-item" href = "index.php?lang=ru">RUS</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row header white">
			<div class="logo col-md-3">
				<div class="header-logo">
					<a href="<?php echo 'index.php?lang='.$lang.''; ?>">
						<img src="public_html/images/logo_omniva_large.png" alt="https://www.omniva.ee/public/banners/pood/logo_717x696.png">
					</a>
				</div>
			</div>

			<div class="title-column col-sm-12 col-md-9 omniva-red-background d-flex justify-content-end align-items-center row">
				<span class="rectangle"></span>
				<h1 class="header-title"><?php echo $json_lang["description"]["title"];?></h1>
			</div>
		</div>
		
		
		<!-- Content -->
		<div class="row justify-content-center">
			<div class="col-xl-4 col-l-5 col-md-6 col-sm-7 col-11">
				<div class="padding-from-header" name="return-form" novalidate="">

					<div class="center form-group justify-text">
						<h2><?php echo $json_lang["description"]["description"];?></h2>
					</div>


					<div class="padding-from-header" name="return-form" novalidate="">
						<div class="center form-group justify-text">
							Lorem ipsum dolor sit amet, consectetur adispiscing elit. Suspendisse euismod mauris at ligula volutpat vulputate vitae ut dolor. Vivamus dignissim id purus quis congue. Sed sid amet mollis metus.
						</div>
				
						<div class="form-group">
							<a class="button button-space btn-lg col-12 omniva-red-background no-border" href="private.php?lang=<?php echo $lang?>">
								<?php 
									echo $json_lang["main_buttons"]["private"];
								?>
							</a>
							<a class="button btn-lg col-12 omniva-red-background no-border" href="business.php?lang=<?php echo $lang?>">
								<?php 
									echo $json_lang["main_buttons"]["business"];
								?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
		
	</body>
</html>
