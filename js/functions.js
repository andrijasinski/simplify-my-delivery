// document.getElementById("submit").addEventListener("load", isFormValid);

var userInputValidations = {  // Here are regexes, limits and error messages if required
    "axa" : { "regex" : /\d+/, "regexError" : "AXA code should contain only digits (from 0 to 9).", 
        "minLen" : "5", "maxLen" : "7", "lenError" : "AXA code must contain from 5 to 7 digits."},
    "name" : {"regex" : "", "regexError" : "", "minLen" : "", "maxLen" : "50", "lenError" : "Maximum length is 50 characters."},
    "address" : {"regex" : "", "regexError" : "", "minLen" : "", "maxLen" : "255", "lenError" : "Maximum length is 255 characters."},
    "city" : {"regex" : /[A-Za-z ]+/, "regexError" : "Only letters and whitespaces are allowed.", 
        "minLen" : "2", "maxLen" : "50", "lenError" : "City length must be from 2 to 50 characters."},
    "zip" : {"regex" : /\d+/, "regexError" : "ZIP code should contain only digits (from 0 to 9).", 
        "minLen" : "", "maxLen" : "10", "lenError" : "Maximum length is 10 characters."},
    "serviceFound" : {"regex" : /[A-Z]+/, "regexError" : "Only uppercase letters allowed.", 
        "minLen" : "2", "maxLen" : "2", "lenError" : "Length is 2 characters."}, 
    "serviceNotFound" : {"regex" : /[A-Z]+/, "regexError" : "Only uppercase letters allowed.", 
        "minLen" : "2", "maxLen" : "2", "lenError" : "Length is 2 characters."} 
};

var buttonAbleDisable = {
    "axa": false, "name": false, "address": false, "city": false, "zip": false
}

function goBack() {
    window.history.back();
}

function validateUserInput(obj) {
    var field = obj.name,
        inpObj = obj.value,
        errorM = "",
        passed = true, 
        possRegex = userInputValidations[field]["regex"],
        possMinLen = userInputValidations[field]["minLen"],
        possMaxLen = userInputValidations[field]["maxLen"];
    // console.log(errorM, "1290412");        
    // console.log(field, inpObj, possRegex, possMinLen, possMaxLen);
    if (possRegex !== "") {
        var re = new RegExp(possRegex);            
        if (!re.test(inpObj)) {
            errorM += " " + userInputValidations[field]["regexError"];
            passed = false;
        }
    }
    // console.log(possMaxLen !== "" && errorM === "");
    if (possMinLen !== "" && errorM === "") {
        // console.log("possMinLen not ''");
        // console.log(errorM, " ", errorM === "");
        // console.log("Entered if statement of else if (possMinLen !== \"\") with field ", field);              
        if (inpObj.length < parseInt(possMinLen) || inpObj.length > parseInt(possMaxLen)) {
            errorM += " " + userInputValidations[field]["lenError"];
            passed = false;
        }
    }
    else if (possMaxLen !== "" && errorM === "") {
        // console.log(possMaxLen !== "" && errorM === "");
        // console.log("Entered if statement of else if (possMaxLen !== \"\") with field ", field); 
        if (inpObj.length < 1) {
            errorM += " Field is required";
        }     
        else if (inpObj.length > parseInt(possMaxLen)) {
            errorM += " " + userInputValidations[field]["lenError"];
            passed = false;
        }
    }
    if (passed) {
        buttonAbleDisable[field] = true;
    }
    isFormValid();
    // console.log(errorM);
    document.getElementById(field+"Error").innerHTML = errorM;
}

function isFormValid() {
    // console.log(buttonAbleDisable);
    var fields = ["axa", "name", "address", "city", "zip", "serviceFound", "serviceNotFound"],
        pass = true;
    
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var correct = buttonAbleDisable[field];

		if (correct === false) { 
			pass = pass && correct;
			break;
		}
    }

    if (pass) {
        document.getElementById("submit").disabled = false;        
    }
    else{
        document.getElementById("submit").disabled = true;
    }
}


function onLoad() {
    var formValid = true,
        formEmpty = true;
        fields = ["axa", "name", "address", "city", "zip", "serviceFound", "serviceNotFound"];

    // console.log("23")
    
    // for (var i = 0; i < fields.length; i++) {
    //     var field = fields[i];
    //     var correct = buttonAbleDisable[field];
    //     formValid = formValid && correct;
    // }

    document.getElementById("submit").disabled = true;    

    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var input = document.getElementById(field).value;
        
        if (input.length != 0) {
            validateUserInput(document.getElementById(field))
        }
    }
}

function clearForm() {
    // console.log("24");
    var clear = confirm("Do you really want to clear form?");
    if (clear) {
        document.getElementById("mainForm").reset();
    }
    return clear;
}

function displayLoading() {
    var x = document.getElementById("submitDIV");
    var y = document.getElementById("loadingDIV");
    if (x.style.display == "none") {
        x.style.display = "block";
        y.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "block";
    }
}