<?php
	if(!isset($_SESSION)) { 
		session_start();
	}
	$file = 2;
	$content = $_SESSION['csv_content'];
	//converts content to ANSI format (for some reason UTF-8 will not display umlaut characters correctly, if CSV is opened in Excel) 
	$data = iconv("UTF-8", "ISO-8859-13", $content);
	
	$rowcount = $_SESSION['rowcount'];
	$csvFirstLine = $_SESSION['csvFirstLine'];
	$csvrowlimit = $_SESSION['csvrowlimit'];
	$csvheaderrow = $_SESSION['csvheaderrow'];
	
	$result = $csvFirstLine . "\r\n;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\r\n" . $csvheaderrow . "\r\n";
	
	$data = explode("|", $data);

	$first = ($file - 1) * $csvrowlimit;
	$last = 1;
	if ($rowcount < $file * $csvrowlimit) {
		$last = $rowcount;
	} else {
		$last = $file * $csvrowlimit;
	}
	
	$filename = "result_" . $_SESSION['timestamp'] . "_" . ($first + 1) . "-" . $last;
	for ($row = 1; $first < $last; $row++) {
		$result = $result . $row . $data[$first++] . "\r\n";
	}
	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename=" . $filename . ".csv");
	echo $result;
?>