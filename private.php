<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Lähima pakiautomaadi leidja</title>
		<link rel="icon" type="image/x-icon" href="./public_html/images/logo_omniva.png">
		<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" 
		integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"-->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<link rel="stylesheet" type="text/css" href="public_html/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="public_html/css/main.css">
		<link rel="stylesheet" type="text/css" href="public_html/css/style.css">

		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>		
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
		<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>

		<!-- <script lang="javascript" src="js/js-xlsx-master/tests/xlsx.full.min.js"></script> -->
		<!-- <script lang="javascript" src="js/excelparser.js"></script> -->
		<script type="text/javascript" src="js/functions.js"></script>

		<?php 
			$urlextension = substr($_SERVER['HTTP_HOST'], -2);
			if($urlextension == "ee") {
				$lang = "et";
			} else if ($urlextension == "lt") {
				$lang = "lt";
			} else if ($urlextension == "lv") {
				$lang = "lv";
			} else {
				$lang = "en";
			}
			$languages = array("en", "ru", "et", "lv", "lt");
			if(isset($_GET['lang']) && in_array($_GET['lang'], $languages)) {
					$lang = $_GET['lang'];
			}
			$str = file_get_contents("./php/lang/lang_{$lang}.json");
			$json_lang = json_decode($str, true);
		?>

	</head>
	<body onload="onLoad()">
		<!-- <script src="//code.jquery.com/jquery-1.11.0.min.js"></script> -->
		<!-- <script type="text/javascript" src="js/bootstrap-filestyle.min.js"> </script> -->

		<?php
			if(!isset($_SESSION)) { 
				session_start();
			}
			ini_set('max_execution_time', 300);
			include_once("php/functions.php");
			$functions = new Functions;
			// define variables and set to empty values
			$name = $axa = $address = $zip = "12345";
			$city = "True";
			$radius = "";
			$nameErr = $axaErr = $addressErr = $cityErr = $zipErr = $fileInputErr = "";
			$radiusErr = "";
			
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$name = $functions->test_input($_POST["name"]);
				$nameErr = $functions->check_user_input($name, "name", $json_lang);

				$axa = $functions->test_input($_POST["axa"]);
				$axaErr = $functions->check_user_input($axa, "axa", $json_lang);

				$address = $functions->test_input($_POST["address"]);
				$addressErr = $functions->check_user_input($address, "address", $json_lang);

				$city = $functions->test_input($_POST["city"]);
				$cityErr = $functions->check_user_input($city, "city", $json_lang);

				$zip = $functions->test_input($_POST["zip"]);
				$zipErr = $functions->check_user_input($zip, "zip", $json_lang);

				if (empty($_POST["radius"]));
				//TODO take radius input into account 
				else if (is_numeric($_POST["radius"]) && strlen($_POST["radius"])<=5)	$radius = $_POST["radius"];//find_closest($_POST["radius"]);
				else if ($_POST["radius"] == "")	$radius = 0;
				else 	$radiusErr = $json_lang['form_errors']["radius_err"];

			}
		?>

		<!-- Header -->
		<div class="row language">
			<div class="col-sm-12">
				<div class="dropdown d-flex justify-content-end align-items-center">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<?php echo strtoupper($lang); ?>
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">
						<a class="dropdown-item" href="private.php?lang=en">EN</a>
						<a class="dropdown-item" href = "private.php?lang=et">ET</a>
						<a class="dropdown-item" href = "private.php?lang=lv">LV</a>
						<a class="dropdown-item" href = "private.php?lang=lt">LT</a>
						<a class="dropdown-item" href = "private.php?lang=ru">RUS</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row header white">
			<div class="logo col-md-3">
				<div class="header-logo">
					<a href="<?php echo 'index.php?lang='.$lang.''; ?>">
						<img src="public_html/images/logo_omniva_large.png" alt="https://www.omniva.ee/public/banners/pood/logo_717x696.png">
					</a>
				</div>
			</div>

			<div class="title-column col-sm-12 col-md-9 omniva-red-background d-flex justify-content-end align-items-center row">
				<span class="rectangle"></span>
				<h1 class="header-title"><?php echo $json_lang["description"]["title"];?></h1>
			</div>
		</div>


		<!-- Content -->
		<div class="row justify-content-center">
			<div class="col-xl-4 col-l-5 col-md-6 col-sm-7 col-11">
				<div class="padding-from-header" name="return-form" novalidate="">

					<p class="cancel-login">
						<a href="<?php echo 'index.php?lang='.$lang.''; ?>">
							<input type="button" class="button" value="<?php echo $json_lang["header_buttons"]["back"];?>"> 
						</a>
					</p>

					<div class="center form-group justify-text">
						<h2><?php echo $json_lang["description"]["description"];?>
						<br>
						<br>
						<?php echo $json_lang["description"]["test_beginning"];?> <a class="link" href="TestExcelWithErrors.xlsx">TestExcelWithErrors.xlsx</a> <?php echo $json_lang["description"]["error_test"];?>
						<br>
						<br>
						<?php echo $json_lang["description"]["test_beginning"];?> <a class="link" href="TestExcel.xlsx">TestExcel.xlsx</a> <?php echo $json_lang["description"]["converting_test"];?>
						</h2>
					</div>
				
					<form id="mainForm" method="post" enctype = "multipart/form-data" action="<?php echo "private.php?lang={$lang}";//echo htmlspecialchars($_SERVER["REQUEST_URI"]);?>">

						<div class="loginform-group clear">

							<!-- AXA Code -->
							<h2><label for="axa" hidden><?php echo $json_lang["fields"]["axa"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="axa" id="axa" value="<?php echo $axa;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["axa"];?>" required hidden>
							<span class="input-error" id="axaError" hidden><?php echo $axaErr;?></span>
					
							<!-- Name -->
							<h2><label for="name" hidden><?php echo $json_lang["fields"]["name"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="name" id="name" value="<?php echo $name;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["name"];?>" required hidden>
							<span class="input-error" id="nameError" hidden><?php echo $nameErr;?></span>
					
							<!-- City -->
							<h2><label for="city" hidden><?php echo $json_lang["fields"]["city"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="city" id="city" value="<?php echo $city;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["city"];?>" required hidden>
							<span class="input-error" id="cityError" hidden><?php echo $cityErr;?></span>
					
							<!-- Address -->
							<h2><label for="address" hidden><?php echo $json_lang["fields"]["address"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="address" id="address" value="<?php echo $address;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["address"];?>" required hidden>
							<span class="input-error"  id="addressError" hidden><?php echo $addressErr;?></span>
					
							<!-- ZIP Code -->
							<h2><label for="zip" hidden><?php echo $json_lang["fields"]["zip"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="zip" id="zip" value="<?php echo $zip;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["zip"];?>" required hidden>

							<span class="input-error" id="zipError" hidden><?php echo $zipErr;?></span>

							<!-- Radius -->
							<br>
							<h2><label for="radius" hidden><?php echo $json_lang["fields"]["radius"];?>:</label></h2>
							<input type="numeric" name="radius" id="radius" value="<?php echo intval($radius);?>" required hidden>
								<span> 
									<a data-toggle="tooltip" title="<?php echo $json_lang["info"]["radius_info"];?>" hidden>
										<img alt="Info here" src="image/info_icon2.png" width="16" height="16">
									</a>
								</span>								
							<span class="input-error"><?php echo $radiusErr;?></span>
							<br>

						</div>
						<br>

						<?php
							if ($_SERVER["REQUEST_METHOD"] != "POST") {
								//displayed before sumbit button is clicked
								echo '<div id="submitDIV" class="center">';
									echo '<img alt="Info here" src="image/info_icon.png" width="16" height="16">';	
									echo " ";															
									//<!-- Browse button -->
									echo '<input type="file" name="uploaded_file" id="file-1" class="inputfile inputfile-1" hidden/>';
									echo '<label for="file-1">';
									echo '<span class="button btn-default btn-file">';									
									echo $json_lang["main_buttons"]["upload"];
									echo '</span></label>';

									echo " ";
										echo '<span>';
										echo '<a data-toggle="tooltip" title="';
										echo $json_lang["info"]["upload_info"];
										echo '">';
										echo '<img alt="Info here" src="image/info_icon2.png" width="16" height="16">';
										echo '</a></span><br><br>';


									//<!-- Submit button -->
									echo '<input class="button" id="submit" type="submit" name="submit" value="';
									echo $json_lang["main_buttons"]["calculate"];
									echo '" onclick="displayLoading()"/>';
								echo '<br><br></div>';
								
								//loading animation displayed after Submit button is clicked
								echo '<div id="loadingDIV" class="center lds-css ng-scope lds-omniva" hidden>';
									//each div represents one vertical bar
									echo '<div></div>';
									echo '<div></div>';
									echo '<div></div>';
								echo '</div><br>';

							} else {
								ob_start();
								$target_file = $_FILES['uploaded_file']['name'];
								$tmp_dir = $_FILES['uploaded_file']['tmp_name'];
								$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
								if($imageFileType != "xls" && $imageFileType != "xlsx") {
									echo "<script>alert(`". $json_lang['form_errors']["type_err"] ."`)</script>";

									echo '<script type="text/javascript">';
										echo 'window.location.replace(\'private.php?lang='.$lang.'\')';
									echo '</script>';
								}
								else {
									$serviceFound = $serviceNotFound = "";
									date_default_timezone_set("Europe/Tallinn");
									$_SESSION['timestamp'] = date("Ymd_Hms") . rand(10,99);
									
									// find_closest requires precise implementation
									$_SESSION['csv_content'] = $functions->find_closest($tmp_dir, $radius, $json_lang, $serviceFound, $serviceNotFound, $_SESSION['timestamp']);
									
									// If XLSX file has some errors, system redirects user to the same page
									if ($_SESSION['csv_content'] == null) {
										echo '<script type="text/javascript">';
											echo 'window.location.replace(\'private.php?lang='.$lang.'\')';
										echo '</script>';
									} else {
										//<!-- Download XLSX button -->
										echo '<div class="center">';
											echo '<a class="button" href="uploads/result_' . $_SESSION['timestamp'] . '.xlsx">';
											echo $json_lang["main_buttons"]["download_xlsx"];
											echo '</a>';
										echo '</div><br><br>';
									}	   
								}
								ob_end_flush();
							}
						?>
					</form>
				</div>
			</div>
		</div>

			<script>
				$('a[data-toggle="tooltip"]').tooltip({
					animated: 'fade',
					placement: 'top',
					html: true
				});
			</script>
		
	</body>

	<script src="js/custom-file-input.js"></script>

</html>
