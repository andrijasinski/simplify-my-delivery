<?php
$I = new AcceptanceTester($scenario);
$I->amOnPage('/business.php?lang=en');
$I->wantToTest('Country acceptance');
$I->see('Country');
$I->selectOption('country','Estonia');
$I->click("city");
$I->see('Estonia');
$I->selectOption('country','Latvia');
$I->click("city");
$I->see('Latvia');
$I->selectOption('country','Lithuania');
$I->click("city");
$I->see('Lithuania');
