<?php
$I = new AcceptanceTester($scenario);
$I->amOnPage('/business.php?lang=en');
$I->wantToTest('AXA Code acceptance');
$I->see('AXA Code');
$I->fillField('AXA Code','1234');
$I->click("name");
$I->see('AXA code must contain from 5 to 7 digits.');
$I->fillField('AXA Code','12345');
$I->click("name");
$I->fillField('AXA Code','asdvcx');
$I->click("name");
$I->see('AXA code should contain only digits (from 0 to 9).');

