<?php
$I = new AcceptanceTester($scenario);
$I->amOnPage('/business.php?lang=en');
$I->wantToTest('Name acceptance');
$I->see('Name');
$I->fillField('Name','1234');
$I->click("city");
$I->fillField('Name','John');
$I->click("city");
$I->fillField('Name','OneTwoThreeFourFiveSixSevenEightNineTenElevenTwelve');
$I->click("city");
$I->see('Maximum length is 50 characters. ');