<?php
$I = new AcceptanceTester($scenario);
$I->amOnPage('/business.php?lang=en');
$I->wantToTest('City acceptance');
$I->see('City');
$I->fillField('City','Tartu');
$I->click("address");
$I->fillField('City','.');
$I->click("address");
$I->see('Only letters and whitespaces are allowed. ');

