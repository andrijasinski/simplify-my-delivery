<?php
$I = new AcceptanceTester($scenario);
$I->amOnPage('/business.php?lang=en');
$I->wantToTest('ZIP acceptance');
$I->see('ZIP Code');
$I->fillField('ZIP Code','.');
$I->click("radius");
$I->see('ZIP code should contain only digits (from 0 to 9).');
$I->fillField('ZIP Code','12345674342342');
$I->click("radius");
$I->see('Maximum length is 10 characters. ');
$I->fillField('ZIP Code','ab');
$I->click("radius");
$I->see('ZIP code should contain only digits (from 0 to 9).');
$I->fillField('ZIP Code','123455');
$I->click("radius");
