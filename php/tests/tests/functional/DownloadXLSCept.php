<?php
$I = new FunctionalTester($scenario);
$I->amOnPage('/business.php?lang=en');
$I->fillField('AXA Code','12345');
$I->fillField('Name','John Smith');
$I->selectOption('country','Estonia');
$I->fillField('City','Narva');
$I->fillField('Address','Tallinna mnt 60 - 33');
$I->fillField('ZIP Code','23001');
$I->fillField('radius','200');
$I->fillField('serviceFound', 'PA');
$I->fillField('serviceNotFound', 'CD');
$I->attachFile('input[name=uploaded_file]', 'TestExcel.xlsx');
$I->click('submit');
sleep(5);
$I->click(['name' => 'xls']);
?>