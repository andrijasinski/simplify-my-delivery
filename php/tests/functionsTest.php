<?php

require "functions.php";
require "lang/lang_en.json";

class FunctionsTest extends PHPUnit_Framework_TestCase
{
    public $functionsInstance;
    
    public function setUp()
    {
        $this->functionsInstance = new Functions();
        $str = file_get_contents("lang/lang_en.json");
        $json_lang = json_decode($str, true);
    }

    public function testAxa(){

        $this->assertEquals('AXA code must contain from 5 to 7 digits.', $this->functionsInstance->check_user_input('1234','axa', $json_lang));
		$this->assertEquals('AXA code must contain from 5 to 7 digits.', $this->functionsInstance->check_user_input('12345678','axa', $json_lang));
		$this->assertEquals('AXA code should contain only digits (from 0 to 9).', $this->functionsInstance->check_user_input('check','axa', $json_lang));
        $this->assertEquals('AXA code is required', $this->functionsInstance->check_user_input('','axa', $json_lang));
        $this->assertEquals('', $this->functionsInstance->check_user_input('509067','axa', $json_lang));
    }

	public function testName(){
        $this->assertEquals('Name is required', $this->functionsInstance->check_user_input('','name', $json_lang));
        $this->assertEquals('Maximum length is 50 characters.', $this->functionsInstance->check_user_input('OneTwoThreeFourFiveSixSevenEightNineTenElevenTwelve','name', $json_lang));
        $this->assertEquals('', $this->functionsInstance->check_user_input('Vse Mayki OU','name', $json_lang));
    }

    public function testAddress(){
        $this->assertEquals('Address is required. Pattern: Street, Building, Apartment.', $this->functionsInstance->check_user_input('','address', $json_lang));
        $this->assertEquals('Maximum length is 255 characters.', $this->functionsInstance->check_user_input('oaklsdfdlasdfkl sdklasdfklasdkljfhsdkl kldadfjklhdklfasdklfhasdjklh ;fiodhi;a sdilj;filsda j;ildfjasdil jfsdlj flasdkj fdah fkldjaskldjfklasdj fljasd;il fjasd;ilj fsdlj fksdja;flsdia fj;ldasjk fd;lsai fejilj asldafkdjas lfhdsjkl fhasdklfjsdklaj fldjk kasjflisdjflkasdj fijdasl fkjsda fjsdlj fasdlka fkjsdaf sdkl fjasdkl fjsdklaj fsdklaj fklasdj fklasdj fklsdjklhaslkej lafkdj fklasdj fkljsdakfjasd ljflasdjiej ;laj;lsdjfka jds;lfj sdklaj fldsi ajse;l jlaskdjfdlaskj fsdlkjfkalsdj flsdja ile jls;jdfl k','address', $json_lang));
        $this->assertEquals('', $this->functionsInstance->check_user_input('Kohtla Puestee 675-234','address', $json_lang));
    }
    
    public function testCity(){
        $this->assertEquals('City is required', $this->functionsInstance->check_user_input('','city', $json_lang));
        $this->assertEquals('Only letters and whitespaces are allowed.', $this->functionsInstance->check_user_input('12344124','city', $json_lang));
        $this->assertEquals('Only letters and whitespaces are allowed.', $this->functionsInstance->check_user_input('Kuala-Lumpur','city', $json_lang));
        $this->assertEquals('Maximum length is 50 characters.', $this->functionsInstance->check_user_input('Bahrein Hussein Allah Putin Groznyi Pskov China Donald Trump Mister Black Star','city', $json_lang));
        $this->assertEquals('City must be more than 2 characters.', $this->functionsInstance->check_user_input('H','city', $json_lang));
        $this->assertEquals('', $this->functionsInstance->check_user_input('Tallinn','city', $json_lang));
    }
    public function testZip(){
        $this->assertEquals('ZIP Code is required', $this->functionsInstance->check_user_input('','zip', $json_lang));
        $this->assertEquals('ZIP code should contain only digits (from 0 to 9).', $this->functionsInstance->check_user_input('jahue','zip', $json_lang));
        $this->assertEquals('Maximum length is 10 characters.', $this->functionsInstance->check_user_input('1234567891011','zip', $json_lang));
        $this->assertEquals('', $this->functionsInstance->check_user_input('51008','zip', $json_lang));
    }

}	