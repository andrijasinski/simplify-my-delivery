<?php
ini_set('max_execution_time', 300);
include_once("libs/Classes/PHPExcel.php");    
class Functions{

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    
    function echo_user_input($data){
        echo "<h2>Your Input:</h2>";
        foreach ($data as $value) {
            echo "$value <br>";
        }
    }
    
    function check_user_input($input, $field, $json_lang){
        switch ($field) {
            case "axa":
                if (empty($input)) { return "AXA code is required";
                } elseif (!preg_match("/^\d+$/", $input)) { return "AXA code should contain only digits (from 0 to 9).";
                } elseif (strlen($input) < 5 || strlen($input) > 7) { return "AXA code must contain from 5 to 7 digits.";

               /* if (empty($input)) { return $json_lang['form_errors']['axa_empty'];
                } elseif (!preg_match("/^\d+$/", $input)) { return $json_lang['form_errors']['axa_content'];
                } elseif (strlen($input) < 5 || strlen($input) > 7) { return $json_lang['form_errors']['axa_length'];*/

                } else { return "";
                } break;
            case "name":
                if (empty($input)) { return "Name is required";
               
                // if (empty($input)) { return $json_lang['form_errors']['name_empty'];
                
                /* A business name may contain varied characters
                } elseif (!preg_match("/^[a-zA-Z ]+$/", $input)) { return "Only letters and white space allowed.";*/
                } elseif (strlen($input) > 50) { return "Maximum length is 50 characters.";
                
                //} elseif (strlen($input) > 50) { return $json_lang['form_errors']['name_length'];
                
                } else { return "";
                } break;
            case "address":
                if (empty($input)) { return "Address is required. Pattern: Street, Building, Apartment.";
                
                //if (empty($input)) { return $json_lang['form_errors']['address_empty'];
                
                /* Address cannot follow any specific pattern. Different examples:
                Taara 1;
                Sõpruse puiestee 10-10;
                J. Liivi 2-404;
                Ärma talu, etc. */
                /*} elseif (!preg_match("/^[A-Z][a-z]+,(\s)?[0-9]+,(\s)?[0-9]+[a-z]*$/", $input)) { return "Address does not match pattern: Street, Building, Apartment.";*/
                } elseif (strlen($input) > 255) { return "Maximum length is 255 characters.";
                
                //} elseif (strlen($input) > 255) { return $json_lang['form_errors']['address_length'];
                
                } else { return "";
                } break;
            case "city":
                if (empty($input)) { return "City is required";
                } elseif (!preg_match("/^[A-Za-z ]+$/", $input)) { return "Only letters and whitespaces are allowed.";
                } elseif (strlen($input) > 50) { return "Maximum length is 50 characters.";

                /*if (empty($input)) { return $json_lang['form_errors']['city_empty'];
                } elseif (!preg_match("/^[A-Za-z ]+$/", $input)) { return $json_lang['form_errors']['city_content'];
                } elseif (strlen($input) > 50) { return $json_lang['form_errors']['city_max_length'];*/

                /* Shortest city/town name in the Baltics is 3 letters. */
                } elseif (strlen($input) < 2) { return "City must be more than 2 characters.";
                
                //} elseif (strlen($input) < 2) { return $json_lang['form_errors']['city_min_length'];
                
                } else { return "";
                } break;
            case 'zip':
                if (empty($input)) { return "ZIP Code is required";
                } elseif (!preg_match("/^\d+$/", $input)) { return "ZIP code should contain only digits (from 0 to 9).";
                } elseif (strlen($input) > 10) { return "Maximum length is 10 characters.";

                /*if (empty($input)) { return $json_lang['form_errors']['zip_empty'];
                } elseif (!preg_match("/^\d+$/", $input)) { return $json_lang['form_errors']['zip_content'];
                } elseif (strlen($input) > 10) { return $json_lang['form_errors']['zip_length'];*/
                } else { return "";
                } break;
			case 'serviceFound':
                if (empty($input)) { return "Service is required";
                } elseif (!preg_match("/^[A-Z]+$/", $input)) { return "Service code length is 2 characters.";
                } elseif (strlen($input) == 2) { return "Service code length is 2 characters.";

                /*if (empty($input)) { return $json_lang['form_errors']['zip_empty'];
                } elseif (!preg_match("/^\d+$/", $input)) { return $json_lang['form_errors']['zip_content'];
                } elseif (strlen($input) > 10) { return $json_lang['form_errors']['zip_length'];*/
                } else { return "";
                } break;
			case 'serviceNotFound':
                if (empty($input)) { return "Service is required";
                } elseif (!preg_match("/^[A-Z]+$/", $input)) { return "Service code length is 2 characters.";
                } elseif (strlen($input) == 2) { return "Service code length is 2 characters.";

                /*if (empty($input)) { return $json_lang['form_errors']['zip_empty'];
                } elseif (!preg_match("/^\d+$/", $input)) { return $json_lang['form_errors']['zip_content'];
                } elseif (strlen($input) > 10) { return $json_lang['form_errors']['zip_length'];*/
                } else { return "";
                } break;

        }
    }
    
    function find_closest($file_name, $radius, $json_lang, $serviceFound, $serviceNotFound, $timestamp) {	//will be implemented correctly later
        //echo $file_name;
        $excel_file = $this->open_excel_file($file_name);
        $indexes = $this->get_data_column_indexes($excel_file);
        $possErrors = $this->validate_excel_input_values($excel_file, $indexes, $json_lang);
        if ($possErrors != "") {
            // echo "<script>alert(\"Your excel file contains several errors:<br>" . $possErrors . "\")</script>";
            $alert_msg = $json_lang["file_errors"]["mismatches"] . $possErrors . $json_lang["file_errors"]["resubmit"];
            echo "<script>alert(`". $alert_msg ."`)</script>";
            //echo "<script>alert(`Your excel file contains several mismatches:\n" . $possErrors . "\nPlease configure your excel file and submit it again.`)</script>";                    
            return null;
        }
        else {
            $terminals = $this->get_terminals();        
            return $this->start_main_process($excel_file, $terminals, $indexes, $radius, $serviceFound, $serviceNotFound, $timestamp);
        }
        // output_excel_as_table($terminals);
    }
    
    function start_main_process($input, $terminals, $indexes, $radius, $serviceFound, $serviceNotFound, $timestamp){
        //TODO Move API KEY to configuration file (create it as well)
        $GOOGLE_API_KEY = 'AIzaSyA0exj142A8-ODXlrXvDPiVlN04gaeR1AE';
        $results = array();
        //TODO link reciever data to terminal data and return an array of them
        
        $terminals_by_country = $this->get_terminals_by_country($terminals);  
        $lastRow = $terminals->getSheet(0)->getHighestRow();
        // echo "<script>console.log('" . $lastRow . "')</script>";    
        $iterator = $indexes['data_starting_point'];
		while ($iterator <= $lastRow) {
            $line = $this->get_line($input, $iterator, $indexes);
            if (empty($line['country'])) { break; }
            $terms = $terminals_by_country[$line['country']];
            if (isset($line)) {
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
                if ($line['address'] != "") {
                    $addr = str_replace("\"", "", $line['address']);
                    $addr = str_replace(" ", "+", $line['address']);
                    $url .= $addr . '+' . $line['zip_code'] . '+';
                }
                else {
                    $url .=  $line['zip_code'] . '+';                
                }
                switch ($line['country']) {
                    case 'EE':
                        $url .= 'Estonia&key=' . $GOOGLE_API_KEY;
                        break;
                    case 'LV':
                        $url .= 'Latvia&key=' . $GOOGLE_API_KEY;
                        break;
                    case 'LT':
                        $url .= 'Lithuania&key=' . $GOOGLE_API_KEY;
                        break;
                }
                echo "<script>console.log('" . $url . "')</script>";
    
                $json = file_get_contents($url);
                $obj = json_decode($json, true);          
                $user_lat = $obj['results'][0]['geometry']['location']['lat'];
                $user_lng = $obj['results'][0]['geometry']['location']['lng'];
                echo "<script>console.log(' Saaja aadress: " . $user_lat . " " . $user_lng . "')</script>";   
                $optimal = intval(2147483647);            
    
                foreach ($terms as $key => $value) {
                    if ($value['type'] == "0") {
                        $distance_terminal_user = $this->distance($user_lat, $user_lng, floatval($value['y_co']), floatval($value['x_co']));
                        //echo "<script>console.log('" . $distance_terminal_user . "')</script>";                          
                        if ($distance_terminal_user < $optimal) {
                            $optimal = $distance_terminal_user;
                            $results[$iterator] = $value;
                        }
                        //echo "<script>console.log('" . $optimal['distance'] . "')</script>";      
                    }
                }
                $results[$iterator]['distance'] = $optimal;
                
                if ($optimal <= $radius || $radius == 0) {
                    echo "<script>console.log(' Lahim pakiautomaat on raadiuses " . $results[$iterator]['y_co'] . " " . $results[$iterator]['x_co'] . " " . $results[$iterator]['distance'] . "')</script>"; 
                }
                else {
                    echo "<script>console.log(' Lahim pakiautomaat ei ole raadiuses " . $results[$iterator]['y_co'] . " " . $results[$iterator]['x_co'] . " " . $results[$iterator]['distance'] . "')</script>";                 
                }      
                $iterator += 1;  
            }      
        }
        return $this->download($input, $results, $indexes, $radius, $serviceFound, $serviceNotFound, $timestamp, $terminals_by_country);
        // return $results;
    }
    
    function distance($lat1, $lng1, $lat2, $lng2){ //klient, avtomat AK
        $earth_radius = 6371000;
    
        $dLat = deg2rad($lat2-$lat1);
        $dLng = deg2rad($lng2-$lng1);
    
        $sindLat = sin($dLat/2);
        $sindLng = sin($dLng/2);
    
        $coss =  cos(deg2rad($lat1)) * cos(deg2Rad($lat2));
        $a = pow($sindLat, 2) + pow($sindLng, 2) * $coss;
    
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
    
        $dist = $earth_radius * $c;
    
        return $dist;
    
    }
    
    function get_terminals_by_country($terminals){
        $terminals_by_country = array(
            'EE' => array(), 
            'LV' => array(),  
            'LT' => array()
        );
        $terminal_indexes = $this->get_terminal_indexes($terminals);
    
        $csvFile = file("http://www.omniva.ee/locations.csv");
        $data = []; 
        foreach ($csvFile as $line) {
            $line = str_replace(",", "", $line);
            $line = str_replace(";", ",", $line);
            $data[] = str_getcsv($line);
        }
        foreach( $data as $k => $columns ){
            switch ($columns[$terminal_indexes['country']]) {
                case 'EE':
                    $arr = array(
                        'name' => $columns[$terminal_indexes['name']],
                        'type' => $columns[$terminal_indexes['type']],        
                        'x_co' => $columns[$terminal_indexes['x_co']],
                        'y_co' => $columns[$terminal_indexes['y_co']],
                        'zip_code' => $columns[$terminal_indexes['zip_code']],
                        'country' => 'EE',
						'city' => $columns[$terminal_indexes['city']]
                    );
                    $terminal_by_country['EE'][] = $arr;
                    // array_push($terminal_by_country['EE'], $arr);
                    break;
                case 'LV':
                    $arr = array(
                        'name' => $columns[$terminal_indexes['name']],
                        'type' => $columns[$terminal_indexes['type']],        
                        'x_co' => $columns[$terminal_indexes['x_co']],
                        'y_co' => $columns[$terminal_indexes['y_co']],
                        'zip_code' => $columns[$terminal_indexes['zip_code']],
                        'country' => 'LV',
						'city' => $columns[$terminal_indexes['city']]
                    );
                    $terminal_by_country['LV'][] = $arr;                
                    // array_push($terminal_by_country['LV'], $arr);
                    break;
                case 'LT':
                    $arr = array(
                        'name' => $columns[$terminal_indexes['name']],
                        'type' => $columns[$terminal_indexes['type']],        
                        'x_co' => $columns[$terminal_indexes['x_co']],
                        'y_co' => $columns[$terminal_indexes['y_co']],
                        'zip_code' => $columns[$terminal_indexes['zip_code']],
                        'country' => 'LT',
						'city' => $columns[$terminal_indexes['city']]
                    );
                    $terminal_by_country['LT'][] = $arr;                
                    // array_push($terminal_by_country['EE'], $arr);
                    break;
            }
        }   
    
        return $terminal_by_country;
    }
    
    function get_terminal_indexes($terminals){
        $worksheet = $terminals->getSheet(0);
        $lastRow = $worksheet->getHighestRow();  
        $lastColumn = $worksheet->getHighestColumn();    
        $lastColumn++;
        $data_column_indexes = array(
            'name' => '',
            'type' => '',        
            'x_co' => '',
            'y_co' => '',
            'zip_code' => '',
            'country' => '' ,
            'city' => '',
            'data_starting_point' => ''
        );
        $columns = preg_split("/;/", $worksheet->getCell('A1')->getValue());
        // echo "<script>console.log('columns " . $columns ."')</script>";    
        // for ($row = 1; $row <= $lastRow; $row++) {
        foreach ($columns as $key => $value) {
            switch (strtolower($value)) {
                case 'name':            $data_column_indexes['name'] = $key; break;
                case 'type':            $data_column_indexes['type'] = $key; break;
                case 'x_coordinate':    $data_column_indexes['x_co'] = $key; break;
                case 'y_coordinate':    $data_column_indexes['y_co'] = $key; break;
                case 'zip':             $data_column_indexes['zip_code'] = $key; break;
                case 'a0_name':         $data_column_indexes['country'] = $key; break;
                case 'a2_name':         $data_column_indexes['city'] = $key; break;
            }
        }
        // print_r($data_column_indexes);
        return $data_column_indexes;
    }
    
    function get_line($excelObj, $row, $indexes){
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();
        $line = array(
            'name' => '',
            'cell_num' => '',
            'email' => '',
            'address' => '',
            'city' => '',
            'zip_code' => '',
            'barcode' => '',
            'country' => '' ,
        );
        
        
        if ($row <= $lastRow) {
            foreach ($indexes as $field => $column) {
                if ($field != 'data_starting_point') {
                    $value = $worksheet->getCell($column.$row)->getValue();
                    $line[$field] = $value;
                }
            }
            // echo "<script>console.log({$line})</script>";        
            return $line;
        }
        return NULL;
    }
    
    function get_terminals() {
        $url = "http://www.omniva.ee/locations.csv";
        $filecontent = file_get_contents($url);
        $tmpfname = tempnam(sys_get_temp_dir(),"tmpcsv");
        file_put_contents($tmpfname,$filecontent);
        return $this->open_excel_file($tmpfname);
    }
    
    function open_excel_file($file_name) {
        $excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
        $excelObj = $excelReader->load($file_name);
        return $excelObj;
    }
    
    function validate_excel_input_values($excelObj, $indexes, $json_lang){
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow(); //get_real_highest_row($excelObj, $indexes['data_starting_point'], $indexes['name']);
        // echo "<script>console.log('" . $lastRow . "')</script>";    
        $err_message = "";
        $error_types = array(
            "Empty city" => array(),
            "Empty zip" => array(),
            "Empty country" => array(),
            "Wrong country" => array(),
            "Mixed zip" => array(),
            "Length zip" => array(),
            "Empty cell" => array(),
            "Empty email" => array(),
            "Wrong cell" => array(),
            "Wrong email" => array()
        );
    
        foreach ($indexes as $field => $column) {
            for ($row = $indexes['data_starting_point']; $row <= $lastRow; $row++) {
                // echo "<script>console.log(\"field: " . $field . ", column: " . $column . ", row: " . $row . "\")</script>";            
                switch ($field) {
                    case 'name': break;
                    case 'cell_num': 
                        $value = $worksheet->getCell($column.$row)->getValue();
                        if (empty($value)) { 
                            array_push($error_types["Empty cell"], $row);   
                        }
                        elseif(!preg_match("/8\d\d\d\d\d\d\d\d/", $value) || !preg_match("/2\d\d\d\d\d\d\d/",$value) || !preg_match("/5\d\d\d\d\d\d/", $value) || !preg_match("/5\d\d\d\d\d\d\d/", $value) ) {
                            array_push($error_types["Wrong cell"], $row);
                        }
                        break;
                    case 'email':
                        $value = $worksheet->getCell($column.$row)->getValue();
                        if (empty($value)){
                            array_push($error_types["Empty email"], $row);
                        }
                        elseif(!preg_match("/\S+@\S+\.\S+/",$value)){
                            array_push($error_types["Wrong email"], $row);
                        }
                        break;
                    case 'address':break;
                    case 'city':
                        /*$value = $worksheet->getCell($column.$row)->getValue();
                        if (empty($value)) { 
                            array_push($error_types["Empty city"], $row);
                            //$err_message .= "Field 'City' on row {$row} is empty.\n"; }
                        }*/
                        break;
                    case 'zip_code':
                        $value = $worksheet->getCell($column.$row)->getValue();
                        if (empty($value)) { 
                            array_push($error_types["Empty zip"], $row);
                            //$err_message .= "Field 'ZIP' on row {$row} is empty.\n"; 
                        }
                        elseif (!preg_match("/^\d+$/", $value)) { 
                            array_push($error_types["Mixed zip"], $row);
                            //$err_message .= "Field 'ZIP' on row {$row} is not valid. ZIP code can contain only numbers.\n";
                        } 
                        elseif (strlen((string) $value) > 10 || strlen((string) $value) < 4) {
                            array_push($error_types["Length zip"], $row);
                            //$err_message .= "Field 'ZIP' on row {$row} is not valid. ZIP code length is wrong.\n";
                        } break;
                    case 'barcode': break;
                    case 'country':
                        $value = $worksheet->getCell($column.$row)->getValue();
                        // echo "<script>console.log('value: " .gettype($value) . "')</script>";
                        if (empty($value)) { 
                            array_push($error_types["Empty country"], $row);
                            //$err_message .= "Field 'Country' on row {$row} is empty.\n"; 
                        }
                        else if (!in_array($value, array("EE", "LV", "LT"))) { 
                            array_push($error_types["Wrong country"], $row);
                            //$err_message .= "Field 'Country' on row {$row} is not valid. Only values EE, LV or LT are allowed.\n";
                        } break;
                }
            }
        }

        foreach ($error_types as $key => $value) {
            if (empty($value))  continue;
            else {
                $comma_sep = implode(",", $value);
                switch ($key) {
                    /*case "Empty city": $err_message .= $json_lang["file_errors"]["city_field"] . $comma_sep . $json_lang["file_errors"]["empty"]; break;*/
                    case "Empty zip": $err_message .= $json_lang["file_errors"]["zip_field"] . $comma_sep . $json_lang["file_errors"]["empty"]; break;
                    case "Empty country": $err_message .= $json_lang["file_errors"]["country_field"] . $comma_sep . $json_lang["file_errors"]["empty"]; break;
                    case "Wrong country": $err_message .= $json_lang["file_errors"]["country_field"] . $comma_sep . $json_lang["file_errors"]["country_content"]; break;
                    case "Mixed zip": $err_message .= $json_lang["file_errors"]["zip_field"] . $comma_sep . $json_lang["file_errors"]["zip_content"]; break;
                    case "Length zip": $err_message .= $json_lang["file_errors"]["zip_field"] . $comma_sep . $json_lang["file_errors"]["zip_length"]; break;
                }
            }
        }
        return $err_message;
    }
    
    function get_real_highest_row($excelObj, $starting_point_row, $column){
        $worksheet = $excelObj->getSheet(0);
        for ($row = $starting_point_row; $row <= $worksheet->getHighestRow(); $row++) {
            $value = $worksheet->getCell($column.$row)->getValue();
            if (empty($value)){
                return $row-1;
            }
        }
    }
    
    function get_data_column_indexes($excelObj){
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();
        // echo "<script>alert(\"" . $lastRow . "\")</script>";    
        $lastColumn = $worksheet->getHighestColumn();
        // echo "<script>alert(\"" . $lastRow . " " . $lastColumn .  "\")</script>";        
        $lastColumn++;
        $data_column_indexes = array(
            'name' => '',
            'cell_num' => '',
            'email' => '',
            'address' => '',
            'city' => '',
            'zip_code' => '',
            'barcode' => '',
            'country' => '' ,
            'data_starting_point' => ''
        );
    
        for ($row = 1; $row <= $lastRow; $row++) {
            for ($column='A'; $column != $lastColumn ; $column++) {
                $value = strtolower($worksheet->getCell($column.$row)->getValue());
                switch ($value) {
                    case 'fullname':    $data_column_indexes['name'] = $column; break;
                    case 'cellnumber':  $data_column_indexes['cell_num'] = $column; break;
                    case 'email':       $data_column_indexes['email'] = $column; break;
                    case 'address':     $data_column_indexes['address'] = $column; break;
                    case 'city':        $data_column_indexes['city'] = $column; break;
                    case 'zip':         $data_column_indexes['zip_code'] = $column; $data_column_indexes['data_starting_point'] = $row+1; break;
                    case 'barcode':     $data_column_indexes['barcode'] = $column; break;
                    case 'country':     $data_column_indexes['country'] = $column; break;
                }
                // echo $value;
            }
        }
        // print_r($data_column_indexes);
        return $data_column_indexes;
    }
    
    function count_excel_rows($excelObj){
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();
        $lastColumn = $worksheet->getHighestColumn();
        $lastColumn++;
		
		//start at row 2, since row 1 is for headers
        $rowcount = 0;
		for ($row = 2; $row <= $lastRow; $row++) {
            $rowvalue = "";
			for ($column='A'; $column != $lastColumn ; $column++) {
                 $rowvalue = $rowvalue . $worksheet->getCell($column.$row)->getValue();
            }
			if ($rowvalue != "") {
				$rowcount++;
			} else {
				return $rowcount;
			}
        }
		return $rowcount;
    }
    
    function output_excel_as_table($excelObj){
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();
        $lastColumn = $worksheet->getHighestColumn();
        $lastColumn++;
    
        echo "<table>";
        for ($row = 1; $row <= $lastRow; $row++) {
            echo "<tr>";
            for ($column='A'; $column != $lastColumn ; $column++) {
                echo "<td>";
                echo $worksheet->getCell($column.$row)->getValue();
                // echo "</td><td>";
                // echo $worksheet->getCell('B'.$row)->getValue();
                echo "</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
    
    function output_excel_as_array($excelObj){
        $worksheet = $excelObj->getSheet(0);
        // $lastRow = $worksheet->getHighestRow();
        $data_array = $worksheet->toArray(null, true, true, false);
        print_r($data_array);
    }
    
    function download($input, $results, $indexes, $radius, $serviceFound, $serviceNotFound, $timestamp, $terminals_by_country){
        $dir = 'uploads';
        
         // create new directory with 744 permissions if it does not exist yet
         // owner will be the user/group the PHP script is run under
         if ( !file_exists($dir) ) {
             $oldmask = umask(0);  // helpful when used in linux server  
             mkdir ($dir, 0744);
         }

        $start_writing_point = $input->getSheet(0)->getHighestColumn();
        $headers_line = $indexes['data_starting_point']-1;        
        $lastColumn = $input->getSheet(0)->getHighestColumn();
        for ($column = $indexes['zip_code']; $column < $lastColumn; $column++) { 
            $value = strtolower($input->getSheet(0)->getCell($column.$headers_line)->getValue());
            if (empty($value)) {
                $start_writing_point = $column;
                break;
            }            
        }
        $input->getSheet(0)->setCellValue($start_writing_point.$headers_line, 'Terminal ZIP');
        $c = $start_writing_point;
        $c++;
        $input->getSheet(0)->setCellValue($c.$headers_line, 'Terminal Name');
        $c++;
        $input->getSheet(0)->setCellValue($c.$headers_line, 'Distance');

        foreach ($results as $line => $terminal) {
            $input->getSheet(0)->setCellValue($start_writing_point.$line, $terminal['zip_code']);
            $c = $start_writing_point;
            $c++;
            $input->getSheet(0)->setCellValue($c.$line, $terminal['name']);
            $c++;
            $input->getSheet(0)->setCellValue($c.$line, intval($terminal['distance']));
        }

        $objWriter = PHPExcel_IOFactory::createWriter($input, 'Excel2007');
        $objWriter->save(str_replace(__FILE__,$dir.'/result_' . $timestamp . '.xlsx',__FILE__));				
		return output_excel_as_csv($input, $radius, $serviceFound, $serviceNotFound, $terminals_by_country);
    }
	
}
function output_excel_as_csv($excelObj, $radius, $serviceFound, $serviceNotFound, $terminals_by_country){
	$worksheet = $excelObj->getSheet(0);
	$lastRow = $worksheet->getHighestRow();
	$lastColumn = $worksheet->getHighestColumn();
	$lastColumn++;
	
	$result = "";

	for ($row = 2; $row <= $lastRow; $row++) {
		$rowdata = "";
		
		//$services is dependent on $phone, $address, $terminalname
		//if $phone not valid, then $serviceNotFound is added
		//if $phone valid, $address available, but no $terminalname, then $serviceNotFound is added
		//if $phone valid, $address available, $terminalname available, then $serviceFound is added
		$service = "";
		
		//additional service: if $phone is valid, then add "ST,"; if $email is valid, then add "SF,"
		$add_service = "";

		$column='A';
		$name = str_replace(";", "", $worksheet->getCell($column.$row)->getValue());
		$column++;
		$phone = str_replace(";", "", $worksheet->getCell($column.$row)->getValue());
		$column++;
		$email = str_replace(";", "", $worksheet->getCell($column.$row)->getValue());
		$column++;
		$address = str_replace(";", "", $worksheet->getCell($column.$row)->getValue());
		$column++;
		$city = str_replace(";", "", $worksheet->getCell($column.$row)->getValue());
		$column++;
		$zip = str_replace(";", "", $worksheet->getCell($column.$row)->getValue());
		$column++;
		$country = str_replace(";", "", $worksheet->getCell($column.$row)->getValue());
		$column++;
		$barcode = str_replace(";", "", $worksheet->getCell($column.$row)->getValue());
		$column++;
		$terminalzip = str_replace(";", "", $worksheet->getCell($column.$row)->getValue());
		$column++;
		$terminalname = str_replace(";", "", $worksheet->getCell($column.$row)->getValue());
		$column++;
		$distance = str_replace(";", "", $worksheet->getCell($column.$row)->getValue());
		
		//phone validation - check if matches country's local requirement for mobile number
		$phonevalid = false;
		$phonetest = str_replace(" ","",$phone);
		$phonetest = str_replace("-","",$phonetest);
		$phonetest = str_replace(".","",$phonetest);
		$phonetest = str_replace("+","",$phonetest);
		$phonetest = str_replace("'","",$phonetest);
		$phonetest = str_replace("(","",$phonetest);
		$phonetest = str_replace(")","",$phonetest);
		if ($country == "EE" && 
			(preg_match("/5\d\d\d\d\d\d/", $phonetest) || preg_match("/5\d\d\d\d\d\d\d/", $phonetest) ||
			preg_match("/8\d\d\d\d\d\d\d\d/", $phonetest)|| preg_match("/3725\d\d\d\d\d\d/", $phonetest) ||
			preg_match("/3725\d\d\d\d\d\d\d/", $phonetest) || preg_match("/3728\d\d\d\d\d\d\d\d/", $phonetest))) $phonevalid = true;
		elseif ($country == "LV" && 
			(preg_match("/2\d\d\d\d\d\d\d/", $phonetest) || preg_match("/3712\d\d\d\d\d\d\d/", $phonetest))) $phonevalid = true;
		elseif ($country == "LT" && 
			(preg_match("/86\d\d\d\d\d\d\d/", $phonetest) || preg_match("/37086\d\d\d\d\d\d\d/", $phonetest))) $phonevalid = true;
				
		//email validation
		$emailvalid = false;
		if (preg_match("/\S+@\S+\.\S+/",$email)) $emailvalid = true;
		
		//set $service
		if ($phonevalid and ($address != "") and ($terminalname != "") and ($radius == 0 || $distance <= $radius)) {
			$service = $serviceFound;
			//set address to terminal
			$terminalcity = "";
			foreach ($terminals_by_country[$country] as $arr) {
				if ($arr['name'] == $terminalname) {
					$terminalcity = $arr['city'];
					break;
				}
			}
			$address = $terminalname;
			$zip = $terminalzip;
			$city = $terminalcity;
		}
		else $service = $serviceNotFound;
		
		//set $add_service
		if ($phonevalid) $add_service = $add_service . "ST,";
		if ($emailvalid) $add_service = $add_service . "SF,";
		
		$rowdata = ";" . $barcode . ";;;" . $service . ";" . $add_service . ";;;;;;;;" . $name . ";" . $address . ";;;;" . $zip . ";" . $city . ";;;" . $country . ";;;" . $phone . ";" . $email . ";;" . $phone . ";" . $email . ";;";

		$result = $result . $rowdata . "|";
	}
	return $result . "|";
}
?>
