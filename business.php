<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Lähima pakiautomaadi leidja</title>
		<link rel="icon" type="image/x-icon" href="./public_html/images/logo_omniva.png">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<link rel="stylesheet" type="text/css" href="public_html/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="public_html/css/main.css">
		<link rel="stylesheet" type="text/css" href="public_html/css/style.css">

		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>		
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
		<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>

		<!-- <script lang="javascript" src="js/js-xlsx-master/tests/xlsx.full.min.js"></script> -->
		<!-- <script lang="javascript" src="js/excelparser.js"></script> -->

		<?php 
			$urlextension = substr($_SERVER['HTTP_HOST'], -2);
			if($urlextension == "ee") {
				$lang = "et";
			} else if ($urlextension == "lt") {
				$lang = "lt";
			} else if ($urlextension == "lv") {
				$lang = "lv";
			} else {
				$lang = "en";
			}
			$languages = array("en", "ru", "et", "lv", "lt");
			if(isset($_GET['lang']) && in_array($_GET['lang'], $languages)) {
					$lang = $_GET['lang'];
			}
			$str = file_get_contents("./php/lang/lang_{$lang}.json");
			$json_lang = json_decode($str, true);
		?>
		
	</head>
	<body onload="onLoad()">
		<!-- <script src="//code.jquery.com/jquery-1.11.0.min.js"></script> -->
		<!-- <script type="text/javascript" src="js/bootstrap-filestyle.min.js"> </script> -->

		<?php
			if(!isset($_SESSION)) { 
				session_start();
			}
			ini_set('max_execution_time', 300);
			include_once("php/functions.php");
			$functions = new Functions;
			// define variables and set to empty values
			$name = $axa = $address = $city = $zip = $radius = $country = $serviceFound = $serviceNotFound =  "";
			$nameErr = $axaErr = $addressErr = $cityErr = $zipErr = $radiusErr = $countryErr = $serviceFoundErr =
				$serviceNotFoundErr = $fileInputErr = "";

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				
				$name = $functions->test_input($_POST["name"]);
				$nameErr = $functions->check_user_input($name, "name", $json_lang);

				$axa = $functions->test_input($_POST["axa"]);
				$axaErr = $functions->check_user_input($axa, "axa", $json_lang);

				$address = $functions->test_input($_POST["address"]);
				$addressErr = $functions->check_user_input($address, "address", $json_lang);

				$city = $functions->test_input($_POST["city"]);
				$cityErr = $functions->check_user_input($city, "city", $json_lang);

				$zip = $functions->test_input($_POST["zip"]);
				$zipErr = $functions->check_user_input($zip, "zip", $json_lang);

				$serviceFound = $functions->test_input($_POST["serviceFound"]);
				$serviceFoundErr = $functions->check_user_input($serviceFound, "service_inside", $json_lang);

				$serviceNotFound = $functions->test_input($_POST["serviceNotFound"]);
				$serviceNotFoundErr = $functions->check_user_input($serviceNotFound, "service_outside", $json_lang);

				if (empty($_POST["radius"]));
				//TODO take radius input into account 
				else if (is_numeric($_POST["radius"]) && intval($_POST["radius"]) != 0 && strlen($_POST["radius"])<=5)	$radius = $_POST["radius"];//find_closest($_POST["radius"]);
				else if ($_POST["radius"] == "")	$radius = 0;
				else 	$radiusErr = $json_lang['form_errors']["radius_err"];

				if (!empty($_POST["country"]))	$country = $_POST["country"];
				else $countryErr = $json_lang['form_errors']["country_err"];

				if (!empty($_POST["serviceFound"]))	$serviceFound = $_POST["serviceFound"];
				else $serviceFoundErr = $json_lang['form_errors']["service_err"];

				if (!empty($_POST["serviceNotFound"]))	$serviceNotFound = $_POST["serviceNotFound"];
				else $serviceNotFoundErr = $json_lang['form_errors']["service_err"];
			}
		?>


		<!-- Header -->
		<div class="row language">
			<div class="col-sm-12">
				<div class="dropdown d-flex justify-content-end align-items-center">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<?php echo strtoupper($lang); ?>
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">
						<a class="dropdown-item" href="business.php?lang=en">EN</a>
						<a class="dropdown-item" href = "business.php?lang=et">ET</a>
						<a class="dropdown-item" href = "business.php?lang=lv">LV</a>
						<a class="dropdown-item" href = "business.php?lang=lt">LT</a>
						<a class="dropdown-item" href = "business.php?lang=ru">RUS</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row header white">
			<div class="logo col-md-3">
				<div class="header-logo">
					<a href="<?php echo 'index.php?lang='.$lang.''; ?>">
						<img src="public_html/images/logo_omniva_large.png" alt="https://www.omniva.ee/public/banners/pood/logo_717x696.png">
					</a>
				</div>
			</div>

			<div class="title-column col-sm-12 col-md-9 omniva-red-background d-flex justify-content-end align-items-center row">
				<span class="rectangle"></span>
				<h1 class="header-title"><?php echo $json_lang["description"]["title"];?></h1>
			</div>
		</div>


		<!-- Content -->
		<div class="row justify-content-center">
			<div class="col-xl-4 col-l-5 col-md-6 col-sm-7 col-11">
				<div class="padding-from-header" name="return-form" novalidate="">

					<p class="cancel-login">
						<a href="<?php echo 'index.php?lang='.$lang.''; ?>">
							<input type="button" class="button" value="<?php echo $json_lang["header_buttons"]["back"];?>"> 
						</a>
					</p>

					<div class="center form-group justify-text">
						<h2><?php echo $json_lang["description"]["description"];?>
						<br>
						<br>
						<?php echo $json_lang["description"]["test_beginning"];?> <a class="link" href="TestExcelWithErrors.xlsx">TestExcelWithErrors.xlsx</a> <?php echo $json_lang["description"]["error_test"];?>
						<br>
						<br>
						<?php echo $json_lang["description"]["test_beginning"];?> <a class="link" href="TestExcel.xlsx">TestExcel.xlsx</a> <?php echo $json_lang["description"]["converting_test"];?>
						</h2>
					</div>
				
					<!-- FORM BEGIN -->
					<form id="mainForm" method="post" enctype = "multipart/form-data" action="<?php echo "business.php?lang={$lang}";//echo htmlspecialchars($_SERVER["REQUEST_URI"]);?>">
					
						<div class="loginform-group clear">

							<!-- AXA Code -->
							<br>
							<h2><label for="axa"><?php echo $json_lang["fields"]["axa"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="axa" id="axa" value="<?php echo $axa;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["axa"];?>" required>
							<a data-toggle="tooltip" title="<?php echo $json_lang["info"]["axa_info"];?>">
								<img alt="Info here" src="image/info_icon2.png" width="16" height="16">
							</a>
							<span class="input-error" id="axaError"><?php echo $axaErr;?></span>
							<br>

							<!-- Name -->
							<br>
							<h2><label for="name"><?php echo $json_lang["fields"]["name"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="name" id="name" value="<?php echo $name;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["name"];?>" required>
							<span class="input-error" id="nameError"><?php echo $nameErr;?></span>
							<br>

							<!-- Country -->
							<br>
							<h2><label for="country"><?php echo $json_lang["fields"]["country"];?><span class="input-error">*</span>:</label></h2>
							<select name="country" id="country" required>
								<option value=""><?php echo $json_lang["field_text"]["select_country"];?></option>
								<option value="EE" <?php echo (isset($_POST['country']) && $_POST['country'] == 'EE')?'selected="selected"':''; ?>><?php echo $json_lang["field_text"]["est"];?></option>
								<option value="LV" <?php echo (isset($_POST['country']) && $_POST['country'] == 'LV')?'selected="selected"':''; ?>><?php echo $json_lang["field_text"]["lv"];?></option>
								<option value="LT" <?php echo (isset($_POST['country']) && $_POST['country'] == 'LT')?'selected="selected"':''; ?>><?php echo $json_lang["field_text"]["lt"];?></option>
							</select>
							<span class="input-error"><?php echo $countryErr;?></span>
							<br>

							<!-- City -->
							<br>
							<h2><label for="city"><?php echo $json_lang["fields"]["city"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="city" id="city" value="<?php echo $city;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["city"];?>" required>
							<span class="input-error" id="cityError"><?php echo $cityErr;?></span>
							<br>

							<!-- Address -->
							<br>
							<h2><label for="address"><?php echo $json_lang["fields"]["address"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="address" id="address" value="<?php echo $address;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["address"];?>" required>

							<span class="input-error"  id="addressError"><?php echo $addressErr;?></span>
							<br>

							<!-- ZIP Code -->
							<br>
							<h2><label for="zip"><?php echo $json_lang["fields"]["zip"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="zip" id="zip" value="<?php echo $zip;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["zip"];?>" required>
							<a data-toggle="tooltip" title="<?php echo $json_lang["info"]["zip_info"];?>">
								<img alt="Info here" src="image/info_icon2.png" width="16" height="16">
							</a>
							<span class="input-error" id="zipError"><?php echo $zipErr;?></span>
							<br>

							<!-- Radius -->
							<br>
							<h2><label for="radius"><?php echo $json_lang["fields"]["radius"];?></label></h2>
							<input type="numeric" name="radius" id="radius" value="<?php echo intval($radius);?>" required>
							<span class="input-error"><?php echo $radiusErr;?></span>
							<br>

							<!-- Service 1 -->
							<br>
							<h2><label for="serviceFound"><?php echo $json_lang["fields"]["service_inside"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="serviceFound" id="serviceFound" value="<?php echo $serviceFound;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["service_inside"];?>" required>
							<a data-toggle="tooltip" title="<?php echo $json_lang["info"]["service_inside_info"];?>">
								<img alt="Info here" src="image/info_icon2.png" width="16" height="16">
							</a>
							<span class="input-error" id="serviceError"><?php echo $serviceFoundErr;?></span>
							<br>
							
							<!-- Service 2 -->
							<br>
							<h2><label for="serviceNotFound"><?php echo $json_lang["fields"]["service_outside"];?><span class="input-error">*</span>:</label></h2>
							<input type="text" name="serviceNotFound" id="serviceNotFound" value="<?php echo $serviceNotFound;?>" onblur="validateUserInput(this)" placeholder="<?php echo $json_lang["field_text"]["service_outside"];?>" required>
							<a data-toggle="tooltip" title="<?php echo $json_lang["info"]["service_outside_info"];?>">
								<img alt="Info here" src="image/info_icon2.png" width="16" height="16">
							</a>
							<span class="input-error" id="serviceError"><?php echo $serviceNotFoundErr;?></span>
						</div>
						
						<span class="input-error"><?php echo $json_lang["description"]["required_fields"];?></span>
						<br><br><br>

						<?php
							if ($_SERVER["REQUEST_METHOD"] != "POST") {
								//displayed before sumbit button is clicked
								echo '<div id="submitDIV" class="center">';
									echo '<img alt="Info here" src="image/info_icon.png" width="16" height="16">';	
									echo " ";	
									//<!-- Browse button -->
									echo '<input type="file" name="uploaded_file" id="file-1" class="inputfile inputfile-1" hidden/>';
									echo '<label for="file-1">';
									echo '<span class="button btn-default btn-file">';									
									echo $json_lang["main_buttons"]["upload"];
									echo '</span></label>';

									echo " ";
										echo '<span>';
										echo '<a data-toggle="tooltip" title="';
										echo $json_lang["info"]["upload_info"];
										echo '">';
										echo '<img alt="Info here" src="image/info_icon2.png" width="16" height="16">';
										echo '</a></span><br><br>';


									//<!-- Submit button -->
									echo '<input class="button" id="submit" type="submit" name="submit" value="';
									echo $json_lang["main_buttons"]["calculate"];
									echo '" onclick="displayLoading()"/>';
								echo '<br><br></div>';
								
								//loading animation displayed after Submit button is clicked
								echo '<div id="loadingDIV" class="lds-css ng-scope lds-omniva" hidden>';
									//each div represents one vertical bar
									echo '<div></div>';
									echo '<div></div>';
									echo '<div></div>';
								echo '</div><br>';

							} 
							else {
								ob_start();
								$target_file = $_FILES['uploaded_file']['name'];
								$tmp_dir = $_FILES['uploaded_file']['tmp_name'];
								$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
								if($imageFileType != "xls" && $imageFileType != "xlsx") {
									echo "<script>alert(`". $json_lang['form_errors']["type_err"] ."`)</script>";

									echo '<script type="text/javascript">';
										echo 'window.location.replace(\'business.php?lang='.$lang.'\')';
									echo '</script>';
								}
								else {
									//checking excel file row count w/o header row until first empty row
									$excel_file = $functions->open_excel_file($tmp_dir);
									$_SESSION['rowcount'] = $functions->count_excel_rows($excel_file);
									$_SESSION['csvrowlimit'] = 500;
									
									$_SESSION['csvFirstLine'] = $axa . ";" . $name . ";" . $address . ";;;;" . $zip . ";" . $city . ";;;" . $country . ";;;;;;;;;;;;;;;;;;;;;";
									$_SESSION['csvheaderrow'] = "ORDER_NR;BAR_CODE;PACKET_UNIT_IDENTIFICATOR;SPEED;SERVICE;ADDITIONAL_SERVICE;WEIGHT;COMMENT;RANSOM;RANSOM_RECEIVER;ACCOUNT_NUMBER;REFERENCE_NUMBER;VALUE;RECEIVER_NAME;STREET;HOUSE;HOUSE_LETTER;HOUSE_FLAT_SUBUNIT;ZIP;CITY;PARISH;COUNTY;COUNTRY;CONTACT_PERSON;CONTACT_PERSON_JOB;CONTACT_PERSON_PHONE;CONTACT_PERSON_EMAIL;CONTACT_PERSON_NOTE;RECEIVER_NOTIFICATION_SMS;RECEIVER_NOTIFICATION_EMAIL;SENDER_NOTIFICATION_SMS;SENDER_NOTIFICATION_EMAIL";
									date_default_timezone_set("Europe/Tallinn");
									$_SESSION['timestamp'] = date("Ymd_Hms") . rand(10,99);
									
									// find_closest requires precise implementation
									$_SESSION['csv_content'] = $functions->find_closest($tmp_dir, $radius, $json_lang, $serviceFound, $serviceNotFound, $_SESSION['timestamp']);
									
									//FOLLOWING SECTION IS FOR TESTING PURPOSES, for testing comment out the $_SESSION['csv_content'] value above
									/*
									$_SESSION['rowcount'] = 1788;
									$_SESSION['csv_content'] = "";
									for ($row = 1; $row <= $_SESSION['rowcount']; $row++) {
										$_SESSION['csv_content'] = $_SESSION['csv_content'] . "row: " . $row . ";;;;;;;dummy;data;;;" . "|";
									}
									$_SESSION['csv_content'] = $_SESSION['csv_content'] . "|";
									*/

									// If XLSX file has some errors, system redirects user to the same page
									if ($_SESSION['csv_content'] == null) {
										echo '<script type="text/javascript">';
											echo 'window.location.replace(\'business.php?lang='.$lang.'\')';
										echo '</script>';										
									} 
									else{
																			
										//<!-- Download CSV button -->
										$csvfilecount = ceil($_SESSION['rowcount'] / $_SESSION['csvrowlimit']);
										if ($csvfilecount == 1) {
											echo '<div class="center">';										
												echo '<a class="button" name = "csv" href="csv1.php" target="_blank">';
												echo $json_lang["main_buttons"]["download_csv"];
												echo '</a>';
											echo '</div><br><br>';
										} 
										else {
											$first = 1;
											$last = $_SESSION['csvrowlimit'];
											for ($csvnumber = 1; $csvnumber <= $csvfilecount; $csvnumber++) {
												$first = 1 + ($csvnumber - 1) * $_SESSION['csvrowlimit'];
												if ($csvnumber < $csvfilecount) {
													$last = $csvnumber * $_SESSION['csvrowlimit'];
												} else {
													$last = $_SESSION['rowcount'];
												}
												echo '<div class="center"';	
													echo '<a class="button" href="csv' . $csvnumber . '.php" target="_blank">';
													echo $json_lang["main_buttons"]["download_csv"] . " (" . $first . "-" . $last . ")";
													echo '</a>';
												echo '</div><br><br>';
											}
										}
									}
									echo '<br>';
								}
								ob_end_flush();
							}
						?>
					</form>
				</div>
			</div>
		</div>

		<script>
			$('a[data-toggle="tooltip"]').tooltip({
				animated: 'fade',
				placement: 'top',
				html: true
			});
		</script>	
	</body>

	<script src="js/custom-file-input.js"></script>
	<script type="text/javascript" src="js/functions.js"></script>

</html>
